var path = require('path');
var nodeExternals = require('webpack-node-externals');
const OptimizeCssAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
var webpack = require('webpack');

/*
module.exports = {
    entry: '../src/js/app.js',
    output: {
        path: __dirname + "/src/js/",
        filename: 'app.bundle.js'
    },
    mode: 'development',
    plugins: [
        new ExtractTextPlugin({
            filename: devMode ? '[name].css' : '[name].[hash].css',
            chunkFilename: devMode ? '[id].css' : '[id].[hash].css',
        })
    ],
    optimization: {
        minimizer: [
            new UglifyJsPlugin({
                cache: true,
                parallel: true,
                extractComments: true
            }),
            new OptimizeCssAssetsPlugin({}),
            new CompressionPlugin()
        ]
    },
    module: {
        rules: [
            {
                test: /\.(jsx|js)?$/,
                exclude: /(node_modules)/,
                use: [{
                    loader: "babel-loader",
                    options: {
                        cacheDirectory: true,
                        presets: ['react', 'es2015']
                    }
                }]
            },
            {
                test: /\.(sc|c)ss$/,
                use: [
                    devMode ? 'style-loader' : ExtractTextPlugin.loader,
                    {
                        loader: 'css-loader',
                        options: {
                            modules: true,
                            importLoaders: 1,
                            localIdentName: 'ko_[sha1:hash:hex:3]'
                        }
                    },
                    'sass-loader'
                ],
            }
        ]
    },
};
 */
var browserConfig = {
    entry: './src/browser/index.js',
    output: {
        path: path.resolve(__dirname, 'public'),
        filename: 'bundle.js',
        publicPath: '/'
    },
    node: {__dirname: true},
    mode: 'development',
    optimization: {
        minimizer: [
            new OptimizeCssAssetsPlugin({}),
            new UglifyJsPlugin()
        ]
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                use: 'babel-loader',
                exclude: /node_modules/
            },
            {
                test: /\.(sc|c)ss$/,
                use: [
                    'style-loader',
                    {loader: 'css-loader', options: {modules: true, importLoaders: 1, localIdentName: 'kt_[sha1:hash:hex:6]'}},
                    'sass-loader',
                    {loader: 'postcss-loader', options: {
                            syntax: 'postcss-scss',
                            plugins: [
                                require('postcss-preset-env')(),
                                require('cssnano')
                            ]
                        }},
                ],
            }
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            __isBrowser__: "true"
        })
    ]
};

var serverConfig = {
    entry: './src/server/index.js',
    target: 'node',
    externals: [nodeExternals()],
    mode: 'development',
    node: {__dirname: true},
    output: {
        path: __dirname,
        filename: 'server.js',
        publicPath: '/'
    },
    optimization: {
        minimizer: [
            new OptimizeCssAssetsPlugin({}),
            new UglifyJsPlugin()
        ]
    },
    module: {
        rules: [
            {
                test: /\.(js)$/,
                use: 'babel-loader',
                exclude: '/node_modules/'
            },
            {
                test: /\.scss$/,
                use: [
                    'node-style-loader',
                    {loader: 'css-loader', options: {modules: true, importLoaders: 1, localIdentName: 'kt_[sha1:hash:hex:6]'}},
                    'sass-loader',
                    {loader: 'postcss-loader', options: {
                            syntax: 'postcss-scss',
                            plugins: [
                                require('postcss-preset-env')(),
                                require('cssnano')
                            ]
                        }}
                ],
            }
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            __isBrowser__: "false"
        })
    ]
};

module.exports = [browserConfig, serverConfig];