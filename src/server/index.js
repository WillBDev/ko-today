import express from "express";
import cors from "cors";
import {renderToString} from "react-dom/server";
import React from "react";
import Layout from "../shared/js/components/Layout";
import {collectInitial} from "node-style-loader/collect";
import DateFormat from "../shared/js/DateFormat";
import markup from "../shared/markup";
import fetch from "node-fetch";
import NotFound from "../shared/NotFound";
import config from "../../config";

const initialStyleTag = collectInitial();
const spdy = require("spdy");
const app = express();
const {OAuth2Client} = require("google-auth-library");
const client = new OAuth2Client(config.client_id);
let sql = require("mysql");
const compression = require("compression");
const fs = require("fs");
const options = {
    key: fs.readFileSync("/etc/letsencrypt/live/kotoday.org/privkey.pem"),
    cert: fs.readFileSync("/etc/letsencrypt/live/kotoday.org/fullchain.pem")
};
const YQL = require("yql");
const connection = sql.createConnection({
    host: "localhost",
    user: config.db_user,
    password: config.db_password,
    database: config.db_name
});

connection.connect(err => {
    if (err) {
        console.log("[mysql error]", err);
    }
});
app.use(cors());
app.use(express.urlencoded({extended: true}));
app.use(express.json());
app.use(require("helmet")());
app.use(compression());
app.use(function (req, res, next) {
    if (!("JSONResponse" in res)) {
        return next();
    }

    res.set("Cache-Control", "public, max-age=600");
    res.json(res.JSONResponse);
});
app.use(express.static("public"));

function fetchDate(date, callback, bool) {
    const query = "SELECT data FROM days WHERE date=" + sql.escape(date);
    let data;
    let fact;
    let weather;
    let queries = 3;
    if (bool) {
        let weatherQuery = new YQL("select * from weather.forecast where woeid in (select woeid from geo.places(1) where text=\"west hartford, ct\")");
        weatherQuery.exec(function (err, res) {
            weather = res;
            queries--;
            if (queries === 0) {
                callback({data, fact, weather});
            }
        });
    }
    else {
        queries = 2;
    }
    connection.query(query, function (err, result) {
        if (err) throw err;
        else if (result[0] === undefined || result[0] === null || result[0] === "" || !result[0].data || JSON.parse(result[0].data).placeholder) {
            let gifs = [
                ["https://media.giphy.com/media/3oKIP9iTS7Ze73m1P2/giphy.gif", [500, 349]],
                ["https://media.giphy.com/media/xT5LMHxhOfscxPfIfm/giphy.gif", [480, 362]],
                ["https://media.giphy.com/media/S24vAZeES2INy/giphy.gif", [250, 250]],
                ["https://media.giphy.com/media/5GoVLqeAOo6PK/giphy.gif", [237, 185]],
                ["https://media.giphy.com/media/WUq1cg9K7uzHa/giphy.gif", [480, 360]],
                ["https://media.giphy.com/media/3o7abKGM3Xa70I7jCU/giphy.gif", [300, 300]],
                ["https://media.giphy.com/media/6nuiJjOOQBBn2/giphy.gif", [300, 172]],
                ["https://media.giphy.com/media/oF5oUYTOhvFnO/giphy.gif", [250, 186]],
                ["https://media.giphy.com/media/vaq9VvnAHlsEo/giphy.gif", [239, 178]],
                ["https://media.giphy.com/media/13k4VSc3ngLPUY/giphy.gif", [500, 310]],
                ["https://media.giphy.com/media/ci0uDcGQtBh0k/giphy.gif", [500, 266]],
                ["https://media.giphy.com/media/26tPghhb310muUkEw/giphy.gif", [480, 360]],
                ["https://media.giphy.com/media/xT8qBv24X8yoyH39Hq/giphy.gif", [400, 223]],
                ["https://media.giphy.com/media/I24hjk3H0R8Oc/giphy.gif", [200, 230]],
                ["https://media.giphy.com/media/14sy6VGAd4BdKM/giphy.gif", [180, 228]],
                ["https://media.giphy.com/media/2xgzOPvrWW7hm/giphy.gif", [500, 281]],
                ["https://media.giphy.com/media/Rcblw5Bkdq8M/giphy.gif", [251, 216]],
                ["https://media.giphy.com/media/MSS0COPq80x68/giphy.gif", [245, 160]],
                ["https://media.giphy.com/media/qPKdzt3x44wy4/giphy.gif", [199, 222]],
                ["https://media.giphy.com/media/LQdYIVhHureCI/giphy.gif", [410, 260]],
                ["https://media.giphy.com/media/3jJEr6TfuoI9y/giphy.gif", [540, 405]],
                ["https://media.giphy.com/media/D8fF2FIOUsSpW/giphy.gif", [245, 145]],
                ["https://media.giphy.com/media/D8fF2FIOUsSpW/giphy.gif", [245, 145]],
                ["https://media.giphy.com/media/13EOgFTzFfiUM/giphy.gif", [800, 450]],
                ["https://media.giphy.com/media/daUOBsa1OztxC/giphy.gif", [499, 281]],
                ["https://media.giphy.com/media/rOTGSPxvJJY7m/giphy.gif", [500, 283]],
                ["https://media.giphy.com/media/vKmHoLAQSKKhW/giphy.gif", [432, 331]],
                ["https://media.giphy.com/media/MWYbIyPpTXYVa/giphy.gif", [500, 383]],
                ["https://media.giphy.com/media/opmIBtljGbwZi/giphy.gif", [500, 282]],
                ["https://media.giphy.com/media/NqF8ziJ0XfrS8/giphy.gif", [480, 411.429]]
            ];
            let gif = gifs[Math.floor(Math.random() * gifs.length)];
            data = {
                placeholder: "No School Today!",
                gif: {url: gif[0], dimensions: gif[1]}
            };
            if (result[0] && result[0].data && JSON.parse(result[0].data)) {
                let item = JSON.parse(result[0].data);
                if (item.placeholder && item.placeholder.length > 0) {
                    data.placeholder = item.placeholder + "!";
                }
            }
            queries--;
            if (queries === 0) {
                callback({data, fact, weather});
            }
        }
        else {
            queries--;
            data = JSON.parse(result[0].data);
            if (queries === 0) {
                callback({data, fact, weather});
            }
        }
    });
    let dateObj = DateFormat.convertToObject(date);
    Number.prototype.pad = function (size) {
        let s = String(this);
        while (s.length < (size || 2)) {
            s = "0" + s;
        }
        return s;
    };

    let fetchURI = "https://kotoday.org/history/" + (dateObj.getMonth() + 1).pad(2) + "-" + (dateObj.getDate()).pad(2) + ".json";
    fetch(fetchURI).then((res) => {
        return res.json();
    }).then((result) => {
        let events = result.data.Events;
        let index = Math.floor(Math.random() * (events.length - 1));
        let item = events[index];
        let link = item.links[0].link;
        queries--;
        fact = {quote: {text: item.text, link}};
        if (queries === 0) {
            callback({data, fact, weather});
        }
    });
}

function getRandomMessage() {
    const messages = [
        "Not officially affiliated with KO",
        "Generating dope stuff",
        "Creating scheduling superheros",
        "Pumping out magic ounce-by-ounce",
        "Loading Fortnite mechanism at optimum efficiency",
        "Esketting everything",
        "Directing robot army to battle site",
        "Checking Supreme site for new drops",
        "Inputting values into the quadratic formula",
        "Contacting the president for approval",
        "Yeet",
        "Mining all the diamonds",
        "Meddling with foreign elections",
        "Mining Bitcoin",
        "Selling insider information",
        "Finishing homework last minute",
        "Exposing Watergate",
        "Exposing the NSA",
        "Trying to find more creative loading messages",
        "Making things EPIC",
        "Burning down the house",
        "Preparing low-orbit ion cannon",
        "Deleting system32",
        "Deploying trojan horse",
        "Catching snorlax",
        "Tracking down shmebulock",
        "Finishing the last season of The Office",
        "Binge watching Friends for the 17th time",
        "Browsing reddit for dankest memes",
        "Succumbing to communism",
        "Reaching ultimate enlightenment",
        "Becoming woke",
        "Making some pasta",
        "Breaking bad",
        "Contacting data broker",
        "Moving to Canada"
    ];
    let index = Math.floor(Math.random() * (messages.length - 1));
    return messages[index];
}

app.get("/", (req, res) => {
    const date = DateFormat.convertToStandard(new Date());
    fetchDate(date, function (result) {
        let dayData = result;
        let data = renderToString(<Layout date={date} data={dayData}/>);
        res.status(200).send(markup(data, initialStyleTag, {
            date: date,
            data: dayData,
            random: getRandomMessage()
        }, false));
    }, true);
});

app.get("/today", (req, res) => {
    const date = DateFormat.convertToStandard(new Date());
    fetchDate(date, function (result) {
        let dayData = result;
        let data = renderToString(<Layout date={date} data={dayData}/>);
        res.status(200).send(markup(data, initialStyleTag, {
            date: date,
            data: dayData,
            random: getRandomMessage()
        }, false));
    }, true);
});

app.get("/day/:date", (req, res) => {
    let date = req.params.date;
    const regex = /^((\d{2}|\d)-(\d{2}|\d)-\d{4})$/;
    if (!regex.test(date)) {
        date = DateFormat.convertToStandard(new Date());
    }
    else {
        //Cleanse (Remove Numerical Padding)
        let obj = DateFormat.convertToObject(date);
        if (isNaN(obj.getTime())) {
            date = DateFormat.convertToStandard(new Date());
        }
        else {
            date = DateFormat.convertToStandard(obj);
        }
    }
    fetchDate(date, function (result) {
        let dayData = result;
        let data = renderToString(<Layout date={date} data={dayData}/>);
        res.status(200).send(markup(data, initialStyleTag, {date, data: dayData, random: getRandomMessage()}, true));
    }, true);
});

app.get("/sitemap", (req, res) => {
    res.type(".xml");
    const query = "SELECT * FROM days";
    let body = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\"><url><loc>https://kotoday.org</loc><changefreq>daily</changefreq></url><url><loc>https://kotoday.org/today</loc><changefreq>daily</changefreq></url>";
    connection.query(query, function (err, result) {
        if (result && result.length > 0) {
            for (let i = 0; i < result.length; i++) {
                let item = result[i];
                let date = item.date;
                body += "<url>";
                body += "<loc>https://kotoday.org/day/" + date + "</loc>";
                body += "<changefreq>daily</changefreq>";
                body += "</url>";
            }
        }
        body += "</urlset>";
        res.status(200).send(body);
    });
});

app.get("/fetch-date/:date", (req, res) => {
    fetchDate(req.params.date, function (data) {
        res.status(200).send(data);
    }, false);
});

app.get("/user-list", (req, res) => {
    const query = "SELECT data, stats FROM users";
    connection.query(query, function (err, result) {
        if (err) throw err;
        else if (result && result.length > 0) {
            let list = "";
            let demographics = {total: result.length, seniors: 0, juniors: 0, sophomores: 0, freshmen: 0};
            for (let i = 0; i < result.length; i++) {
                let item = JSON.parse(result[i].data);
                let end = "<br>";
                if (result[i].stats && result[i].stats !== "") {
                    let date = new Date(JSON.parse(result[i].stats).last_online);
                    let months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                    let hours = date.getHours();
                    let suffix = "AM";
                    if (hours > 12) {
                        hours -= 12;
                        suffix = "PM";
                    }
                    end = "<br> Last Online: " + months[date.getMonth()] + ", " + date.getDate() + " " + date.getFullYear() + " at " + hours + ":" + date.getMinutes() + ":" + date.getSeconds() + suffix + "<br>";
                }

                function getTitle(email) {
                    email = email.split(".");
                    if (email.length === 4) {
                        let date = new Date();
                        const year = parseInt(email[2].split("@kingswoodoxford.org")[0]);
                        const remaining = year - parseInt(String(date.getFullYear()).substring(4, 2));
                        if (date.getMonth() > 0 && date.getMonth() < 5) {
                            if (remaining === 3) {
                                demographics.freshmen++;
                                return "Freshman, '" + year;
                            }
                            else if (remaining === 2) {
                                demographics.sophomores++;
                                return "Sophomore, '" + year;
                            }
                            else if (remaining === 1) {
                                demographics.juniors++;
                                return "Junior, '" + year;
                            }
                            else if (remaining === 0) {
                                demographics.seniors++;
                                return "Senior, '" + year;
                            }
                            else {
                                return "Middle Schooler, '" + year;
                            }
                        }
                        else if (remaining === 4) {
                            demographics.freshmen++;
                            return "Freshman, '" + year;
                        }
                        else if (remaining === 3) {
                            demographics.sophomores++;
                            return "Sophomore, '" + year;
                        }
                        else if (remaining === 2) {
                            demographics.juniors++;
                            return "Junior, '" + year;
                        }
                        else if (remaining === 1) {
                            demographics.seniors++;
                            return "Senior, '" + year;
                        }
                        else {
                            return "Middle Schooler, '" + year;
                        }
                    }
                    else {
                        return "Faculty";
                    }
                }

                let title = (item.email) ? getTitle(item.email) : "";
                list += "<b>" + item.name + " ( " + title + " )</b>" + end + "<br>";
            }
            list = "Displaying Users (" + demographics.total + ")[ " + Math.round((demographics.freshmen / demographics.total) * 100) + "% Freshmen, " + Math.round((demographics.sophomores / demographics.total) * 100) + "% Sophomores, " + Math.round((demographics.juniors / demographics.total) * 100) + "% Juniors, " + Math.round((demographics.seniors / demographics.total) * 100) + "% Seniors, " + Math.round(((demographics.total - (demographics.freshmen + demographics.sophomores + demographics.juniors + demographics.seniors)) / demographics.total) * 100) + "% Faculty ]:<br><br>" + list;
            res.status(200).send(list);
        }
    });
});

async function verify(token) {
    const ticket = await client.verifyIdToken({
        idToken: token,
        audience: "640618951759-ghcu52qf2gle28rbm9rqom2115t6sui5.apps.googleusercontent.com",
    });
    return ticket.getPayload()["sub"];
}

app.post("/fetch-user", (req, res) => {
    const defaultBody = {
        name: req.body.name, email: req.body.email, periods: []
    };
    const statsBody = {last_online: Date.now()};
    verify(req.body.id_token).catch(() => {
        res.status(200).send(defaultBody);
    }).then((data) => {
        const query = "SELECT data FROM users WHERE id=" + sql.escape(data);
        connection.query(query, function (err, result) {
            if (err) throw err;
            else if (result && result.length > 0) {
                let obj = JSON.parse(result[0].data);
                const statsQuery = "UPDATE users SET stats=" + sql.escape(JSON.stringify(statsBody)) + " WHERE id=" + sql.escape(data);
                connection.query(statsQuery);
                res.status(200).send(result[0].data);

            }
            else {
                const query = "INSERT INTO users VALUES(" + sql.escape(data) + ", " + sql.escape(JSON.stringify(defaultBody)) + ", " + sql.escape(JSON.stringify(statsBody)) + ")";
                connection.query(query, function () {
                    res.status(200).send(defaultBody);
                });
            }
        });
    });
});


app.post("/save-periods", (req, res) => {
    verify(req.body.id_token).then((data) => {
        const query = "UPDATE users SET data=" + sql.escape(JSON.stringify(req.body.user)) + " WHERE id='" + data + "'";
        connection.query(query, function (err) {
            if (err) throw err;
            else {
                res.status(200).send(req.body);
            }
        });
    });
});

app.get("/404", function (req, res) {
    res.status(404).send(NotFound(), 404);
});

app.get("*", function (req, res) {
    res.redirect("/404");
});
app.listen(8080);
spdy.createServer(options, app).listen(8443);