import {minify} from "html-minifier";

export default () => {
    return minify(`
    <!DOCTYPE html>
         <html lang="en">
         <head>
            <meta name="viewport" charset="utf-8" content="width=device-width">
            <meta name="description" content="A student-made daily scheduler website for the Kingswood Oxford School. It shows the block schedule for every day of the year, aswell as other additional information to help guide students through their day effectively.">
            <meta content="width=device-width,initial-scale=1" name=viewport>
            <meta name="robots" content="noodp,noydir" />
            <meta property="og:title" content="KO Today | 404"/>
            <meta property="og:description" content="A student-made daily scheduler website for the Kingswood Oxford School. It shows the block schedule for every day of the year, aswell as other additional information to help guide students through their day effectively."/>
            <meta property="og:url" content="kotoday.org" />
            <meta property="og:type" content="website" />
            <meta property="og:image" content="/img/banner.png">
            <meta property="twitter:card" content="summary_large_image">
            <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel=stylesheet>
            <link rel="apple-touch-icon" sizes="180x180" href="/img/favicon//apple-touch-icon.png">
            <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
            <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon//favicon-16x16.png">
            <meta name="application-name" content="KOToday"/>
            <meta name="msapplication-square70x70logo" content="/img/favicon/mstile-70x70.png" />
            <meta name="msapplication-square150x150logo" content="/img/favicon/mstile-150x150.png" />
            <meta name="msapplication-wide310x150logo" content="/img/favicon/mstile-310x150.png" />
            <meta name="msapplication-square310x310logo" content="/img/favicon/mstile-310x310.png" />
            <link rel="manifest" href="/manifest.json">
            <meta name="msapplication-TileColor" content="#FF1744">
            <meta name="msapplication-TileImage" content="/img/favicon/favicon/mstitle-144x144.png">
            <meta name="theme-color" content="#FF1744">
            <title>404 | KO Today</title>
            <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900|Material+Icons" type="text/css">
            <style>
                :root {
                  --black: #252525;
                  --light-black: #333333;
                  --background-primary: #F4F4F4;
                  --grey: #858585;
                  --nav-grey: #999999;
                  --red: #FF1744;
                  --dark-red: #a51328;
                  --white: #fff;
                  --medium: 500;
                  --bold: 700;
                  --regular: 400;
                  --light: 300;
                  --extra-bold: 900;
                  --nav-height: 60px;
                  --shadow: 0px 3px 6px 0px rgba(37, 37, 37, 0.25);
                }
                body,html {
                background: var(--background-primary);
                margin:0;
                padding:0;
                }
                
                h1 {
                font-weight: var(--bold);
                color: var(--black);
                font-size: 36px;
                }
                
                h1, p, a {
                margin:0;
                font-family: 'Roboto', 'sans-serif';
                }
                
                p {
                font-weight: var(--regular);
                color: var(--grey);
                }
                
                div {
                text-align: center;
                margin-top: 25vh;
                }
                
                a {
                background: var(--red);
                border-radius: 4px;
                font-size: 14px;
                font-weight: var(--medium);
                text-transform: uppercase;
                padding: 14px;
                box-shadow: var(--shadow);
                color: var(--white);
                display: inline-block;
                text-decoration: none;
                margin-top: 16px;
                transition: 250ms background-color cubic-bezier(0.0, 0.0, 0.2, 1);
                }
                
                a:active {
                background: var(--dark-red);
                }
                
                svg {width: 300px; margin-bottom: 15px}
                
                .a {
                fill: var(--red);
                }
                .b {
                fill: var(--black);
                }
                
                
            </style>
         </head>
         <body>
            <div>
            <svg id="Layer_1" data-name="Layer 1" xmlns="https://www.w3.org/2000/svg"
                                     viewBox="0 0 208.82 69.12">
                                    <path id="path" class="a"
                                          d="M49.62,6.19a6.35,6.35,0,0,1,6.2,6.2V55.81a6.35,6.35,0,0,1-6.2,6.2H6.2a5.61,5.61,0,0,1-4.34-1.86A6.08,6.08,0,0,1,0,55.81V12.39a6.35,6.35,0,0,1,6.2-6.2h3V0h6.18V6.19H40.28V0h6.19V6.19Zm0,49.63V21.73H6.19V55.82Z"/>
                                    <path class="a"
                                          d="M16.63,41.53,14.5,43.87v5.69H9.3V28h5.2V37.5l1.81-2.74L21,28h6.43L20.1,37.53l7.29,12H21.23Z"/>
                                    <path class="a"
                                          d="M46.53,39.22a12.76,12.76,0,0,1-1.17,5.59A8.67,8.67,0,0,1,42,48.55a10,10,0,0,1-9.81,0A8.57,8.57,0,0,1,28.9,45a12.29,12.29,0,0,1-1.26-5.41V38.34a12.7,12.7,0,0,1,1.17-5.59A8.61,8.61,0,0,1,32.14,29,9.89,9.89,0,0,1,42,29a8.86,8.86,0,0,1,3.34,3.71,12.53,12.53,0,0,1,1.21,5.53Zm-5.29-.91a9,9,0,0,0-1.08-4.88,3.44,3.44,0,0,0-3.07-1.67q-3.92,0-4.14,5.87v1.59A9.39,9.39,0,0,0,34,44.1a3.45,3.45,0,0,0,3.13,1.7,3.39,3.39,0,0,0,3-1.68,9,9,0,0,0,1.09-4.81Z"/>
                                    <path class="b"
                                          d="M93.06,20.1H81.87v36H72.7v-36h-11V12.81H93.06Z"/>
                                    <path class="b"
                                          d="M90.39,39q0-7.32,3.61-11.49t10-4.16q6.43,0,10,4.16t3.61,11.55V41.1q0,7.35-3.58,11.48t-10,4.14q-6.47,0-10.06-4.15T90.39,41Zm8.83,2.11q0,8.63,4.82,8.63,4.44,0,4.79-7.2l0-3.54q0-4.41-1.28-6.53a4,4,0,0,0-3.6-2.13,3.9,3.9,0,0,0-3.5,2.13Q99.22,34.57,99.22,39Z"/>
                                    <path class="b"
                                          d="M121.92,39.05q0-7.92,2.8-11.82a9.4,9.4,0,0,1,8.14-3.89,8,8,0,0,1,6.47,3.21V10.43h8.86V56.12h-8l-.4-3.27a8.2,8.2,0,0,1-7,3.87,9.33,9.33,0,0,1-8.07-3.87Q122,49,121.92,41.48Zm8.83,2.11q0,4.76,1.06,6.65a3.75,3.75,0,0,0,3.57,1.89,4.33,4.33,0,0,0,4-2.24V32.77a4.21,4.21,0,0,0-3.92-2.38,3.81,3.81,0,0,0-3.54,1.87q-1.12,1.88-1.12,6.67Z"/>
                                    <path class="b"
                                          d="M170.64,56.12a11.25,11.25,0,0,1-.75-2.5,7.61,7.61,0,0,1-6.4,3.1,9.72,9.72,0,0,1-7-2.68,9.18,9.18,0,0,1-2.78-6.93,9.56,9.56,0,0,1,3.37-7.83q3.38-2.76,9.71-2.82h2.68V33.87a4.7,4.7,0,0,0-.78-3.07,2.86,2.86,0,0,0-2.27-.89c-2.2,0-3.29,1.23-3.29,3.69h-8.8a9.15,9.15,0,0,1,3.49-7.36,13.4,13.4,0,0,1,8.85-2.9q5.53,0,8.57,2.75c2,1.83,3,4.46,3,7.87V49.1a15.22,15.22,0,0,0,1.24,6.52v.5Zm-5.19-6a5.2,5.2,0,0,0,2.49-.56,4.2,4.2,0,0,0,1.52-1.34v-6.7h-2.12a4.61,4.61,0,0,0-3.52,1.37,5.14,5.14,0,0,0-1.29,3.66Q162.53,50.11,165.45,50.11Z"/>
                                    <path class="b"
                                          d="M195,42.11l4.48-18.18h9.39l-11.07,37c-1.66,5.45-4.77,8.18-9.36,8.18a13.77,13.77,0,0,1-3.6-.56V62l1,0a5.23,5.23,0,0,0,3.1-.76,4.75,4.75,0,0,0,1.57-2.63l.68-2.17-9.7-32.52h9.45Z"/>
            </svg>
            <h1>Wow...This is embarrassing!</h1>
            <p>Error 404: It looks like the page that you were looking for doesn't exist :/</p>
            <a href="/">Return Home</a>
            </div>
         </body>
         <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-115170228-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
        
          gtag('config', 'UA-115170228-1');
        </script>
    </html>
    `, {
        removeAttributeQuotes: true,
        useShortDoctype: true,
        sortAttributes: true,
        removeTagWhitespace: true,
        removeRedundantAttributes: true,
        removeOptionalTags: true,
        removeComments: true,
        minifyURLs: true,
        collapseWhitespace: true,
        customAttrAssign: true,
        customEventAttributes: true,
        minifyCSS: true
    });
}