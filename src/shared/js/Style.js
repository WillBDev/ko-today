import classNames from 'classnames/bind';
import style from '../css/style.scss';
import React from "react";
const cx = classNames.bind(style);
export default class Style {
    static parse(className) {
        return cx(className);
    }
}