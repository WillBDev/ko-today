export default class Utils {
    static convertToStandard(date) {
        if (date instanceof Date) { //Object
            return date.getMonth()+1 + "-" + date.getDate() + "-" + date.getFullYear();
        }
        else if (date.includes("/")) { //Safe
            let array = date.split("/");
            return array[1] + "-" + array[2] + "-" + array[0];
        }
        else {
            return date;
        }
    }

    static convertToSafe(date) {
        if (date instanceof Date) {
            return date.getFullYear() + "/" + (date.getMonth()+1) + "/" + date.getDate() + "  00:00:00";
        }
        else if (date.includes("-")) {
            let array = date.split("-");
            return array[2] + "/" + array[0] + "/" + array[1] + " 00:00:00";
        }
        else {
            return date;
        }
    }

    static convertToObject(date) {
        if (!(date instanceof Date)) {
            if (date.includes("-")) { //Standard
                return new Date(this.convertToSafe(date));
            }
            else if (date.includes("/")) { //Safe
                return new Date(date);
            }
        }
        else {
            return date;
        }
    }

    static normalizeStandard(time) {
        let arr = time.split(":");
        let hour = parseInt(arr[0]);
        let minutes = arr[1];
        let suffix = (hour >= 12) ? "PM" : "AM";
        if (hour > 12) {
            hour = hour-12;
        }
        return hour + ":" + minutes + suffix;
    }

    static standardToDate(time) {
        let base = new Date();
        return new Date(base.getFullYear() + "/" + (base.getMonth()+1) + "/" + base.getDate() + " " + time + ":00");
    }
    static compare(date1, date2) {
        date1 = this.standardToDate(date1);
        date2 = this.standardToDate(date2);

        if (date1 > date2) {
            return (date1.getUTCHours()*60) - (date2.getUTCHours()*60) + date1.getMinutes()-date2.getMinutes();
        }
        else {
            return (date2.getUTCHours()*60) - (date1.getUTCHours()*60) + date2.getMinutes()-date1.getMinutes();
        }
    }

    static preciseCompare(date, max) {
        function isFloat(n){
            return Number(n) === n && n % 1 !== 0;
        }
        max = this.standardToDate(max);
        if (max > date) {
            let totalMinutes = ((max.getUTCHours()-date.getUTCHours())*60) + (max.getMinutes() - date.getMinutes())-1;
            let seconds = -date.getSeconds() + 60;
            let raw= "";
            if (totalMinutes >= 60) {
                let divided = totalMinutes/60;
                if (isFloat(divided)) {
                    let parts = divided.toString().split(".");
                    let hours = parts[0];
                    let minutes = Math.round(parseFloat("." + parts[1])*60);
                    raw = hours + "h" + " " + minutes + "m" + " " + seconds + "s";
                }
                else {
                    raw += divided + "h" + " " + seconds + "s";
                }
            }
            else if (totalMinutes !== 0) {
                raw += totalMinutes + "m" + " " + seconds + "s";
            }
            else {
                raw += seconds + "s";
            }
            return raw;
        }
    }
}