import React from "react";
import Style from "../Style";
import DateFormat from "../DateFormat";

export default class Header extends React.Component {
    render() {
        let date = DateFormat.convertToObject(this.props.date);
        let days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        let dayName = days[date.getDay()];
        let months = ['January', "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        let monthName = months[date.getMonth()];
        let dayNumber = date.getDate().toString();
        let target = dayNumber;
        let suffix = "st";
        if (dayNumber.length !== 1) {
            target = dayNumber[dayNumber.length - 1];
        }
        if (parseInt(target) === 2) {
            suffix = "nd";
        }
        else if (parseInt(target) === 3) {
            suffix = "rd";
        }
        else {
            suffix = "th";
        }
        let year = date.getFullYear();
        return (
            <header>
                <div className={Style.parse("header-content")}>
                    <h1>{dayName}</h1>
                    <span>{monthName} {dayNumber}<sup>{suffix}</sup>, {year}</span>
                    <p>{this.props.quote.text}</p>
                    <a draggable="false" rel="noreferrer" className={Style.parse("button")}
                       href={this.props.quote.link} target="_blank" title={"Learn more about this event!"}>Learn
                        More</a>
                </div>
                <div className={Style.parse("header-navigation")}>
                    <a rel="canonical" title={"Go to previous day!"} draggable="false" onClick={(e) => {
                        e.preventDefault();
                        this.props.goTo(-1)
                    }} className={Style.parse("material-icon")}
                       id={Style.parse("prev")} href={"https://kotoday.org/day/" + DateFormat.convertToStandard(date.addDays(-1))}>chevron_left</a>
                    <a rel="canonical" title={"Go to next day!"} draggable="false" onClick={(e) => {
                        e.preventDefault();
                        this.props.goTo(1)
                    }} className={Style.parse("material-icon")}
                       id={Style.parse("next")} href={"https://kotoday.org/day/" + DateFormat.convertToStandard(date.addDays(1))}>chevron_right</a>
                </div>
            </header>
        );
    }
}