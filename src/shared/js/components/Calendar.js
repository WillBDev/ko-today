import React from "react";
import Style from "../Style";
import DateFormat from "../DateFormat";

export default class Calendar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {date: DateFormat.convertToObject(props.date)};
    }
    
    render() {
        let selectedDate = this.state.date;
        const dayLabels = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
        const monthLabels = ['January', 'February', 'March', 'April',
            'May', 'June', 'July', 'August', 'September',
            'October', 'November', 'December'];

        const monthLengths = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
        const firstDay = new Date(selectedDate.getFullYear(), selectedDate.getMonth(), 1);
        const startingDay = firstDay.getDay();
        let monthLength = monthLengths[selectedDate.getMonth()];

        if (selectedDate.getMonth() === 1) {
            if ((selectedDate.getFullYear() % 4 === 0 && selectedDate.getFullYear % 100 !== 0) || selectedDate.getFullYear() % 400 === 0) {
                monthLength = 29;
            }
        }
        const monthName = monthLabels[selectedDate.getMonth()];
        let labels = [];
        for (let i = 0; i < 7; i++) {
            labels.push((
                <p className={Style.parse("day-labels")}>
                    {dayLabels[i]}
                </p>
            ));
        }
        let day = 1;
        let days = [];
        let prev = new Date(selectedDate.getFullYear(), selectedDate.getMonth(), 0);
        let prevMonthDays = prev.getDate();
        let $this = this;
        function genDay(date, number) {
            let str = (date.getMonth()+1) + "-" + number + "-" + date.getFullYear();
            let base = Style.parse("day");
            if (number === (new Date()).getDate() && DateFormat.convertToStandard(new Date()) === ((date.getMonth()+1) + "-" + number + "-" + date.getFullYear())) {
                base += " " + Style.parse("current");
            }
            else if (number === DateFormat.convertToObject($this.props.date).getDate() && DateFormat.convertToStandard(date) === $this.props.date) {
                base += " " + Style.parse("selected");
            }
            else if (DateFormat.convertToStandard(date) !== DateFormat.convertToStandard(selectedDate)) {
                base += " " + Style.parse("filler");
            }
            return (
                <a draggable={false} onClick={(e)=> {e.preventDefault(); document.getElementsByTagName("html")[0].style.overflowY = "auto"; $this.props.changeDayState(str)}} href={"https://kotoday.org/day/" + str} className={base}>
                        {number}
                </a>
            )
        }
        for (let i = startingDay; i > 0; i--) {
            let number = prevMonthDays - i + 1;
            days.push(genDay(prev, number));
        }
        for (let i = 0; i < 9; i++) {
            for (let j = 0; j < 7; j++) {
                if (day <= monthLength && (i > 0 || j >= startingDay)) {
                    if (day === (new Date()).getDate() && DateFormat.convertToStandard(new Date()) === ((selectedDate.getMonth()+1) + "-" + day + "-" + selectedDate.getFullYear())) {
                        days.push(genDay(selectedDate, day));
                    }
                    else if (day === selectedDate.getDate()) {
                        days.push(genDay(selectedDate, day));
                    }
                    else {
                        days.push(genDay(selectedDate, day));
                    }
                    day++;
                }

            }
            if (day > monthLength) {
                let endingDay = 6 - new Date(selectedDate.getFullYear(), selectedDate.getMonth() + 1, 0).getDay();
                if (days.length < 36) {
                    endingDay += 7;
                }
                let next = new Date(selectedDate.getFullYear(), selectedDate.getMonth() + 1, 1);
                let nextDays = next.getDate();
                for (let i = 0; i < endingDay; i++) {
                    let number = nextDays + i;
                    days.push(genDay(next, number));
                }
                break;
            }
        }

        function chunkArray(myArray, chunk_size){
            let index = 0;
            let arrayLength = myArray.length;
            let tempArray = [];

            for (index = 0; index < arrayLength; index += chunk_size) {
                tempArray.push(myArray.slice(index, index+chunk_size));
            }

            return tempArray;
        }
        days = chunkArray(days, 7);
        let final = [];
        for (let i=0; i < days.length; i++) {
            final.push((<div className={Style.parse("row")}>{days[i]}</div>));
        }

        function progress(distance) {
            let current = $this.state.date;
                function addMonths (date, count) {
  if (date && count) {
    var m, d = (date = new Date(+date)).getDate()

    date.setMonth(date.getMonth() + count, 1)
    m = date.getMonth()
    date.setDate(d)
    if (date.getMonth() !== m) date.setDate(0)
  }
  return date
}
            $this.setState({date: addMonths(current, distance)});
        }

        return (
            <div className={Style.parse("calendar")}>
                <a onClick={(e) => {e.preventDefault(); progress(-1)}} className={Style.parse("calendar-prev") + " " + Style.parse("material-icon")}>chevron_left</a>
                <a onClick={(e) => {e.preventDefault(); progress(1)}} className={Style.parse("calendar-next") + " " + Style.parse("material-icon")}>chevron_right</a>
                <div className={Style.parse("row") + " " + Style.parse("calendar-header")}>
                    <p>{monthName} {selectedDate.getFullYear()}</p>
                </div>
                <div className={Style.parse("row")}>
                    {labels}
                </div>
                {final}
            </div>
        );
    }
}