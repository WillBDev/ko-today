import React from "react";
import Style from "../Style";
import {fetchDate} from "../apis";
import DateFormat from "../DateFormat";
let controller;
let signal;
let timer;
export default class WeatherModule extends React.Component {
    constructor(props) {
        super(props);
        this.state = {data: this.props.weather};
    }

    getForecast() {
        let $this = this;
        const arr = this.state.data.query.results.channel.item.forecast;
        let items = [];
        for (let i=0; i < 7; i++) {
            let str = DateFormat.convertToStandard(new Date(arr[i].date));
            let bool = (str === this.props.date);
            let active = (bool) ? " " + Style.parse("active") : "";
            items.push(
                <a onClick={()=>{$this.props.goTo(str)}} className={Style.parse("weather-forecast-item") + active }>
                    <b>{arr[i].day}</b>
                    <i className={"wi wi-yahoo-" + arr[i].code}/>
                    <span>{arr[i].low}&deg; / {arr[i].high}&deg;</span>
                </a>
            );
        }
        return items;
    }

    componentDidMount() {
        let $this = this;
        function fetchWeather($this) {
            if (controller !== undefined) {
                controller.abort();
            }

            if (timer) {
                clearTimeout(timer);
            }

            if ("AbortController" in window) {
                controller = new AbortController;
                signal = controller.signal;
            }
            fetch("https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22west%20hartford%2C%20ct%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys", {signal}).then((res)=>{return res.json();}).then((final) => {
                $this.setState({data: final});
            }).catch(err => {
                if (err.name !== 'AbortError') {
                    timer = setTimeout(fetchWeather, 1000);
                }
            });
        }
        setInterval(()=>{fetchWeather($this)}, 300000);
    }

    degreesToDirection() {
        let degrees = this.state.data.query.results.channel.wind.direction;
        if (degrees === 0 && degrees < 45) {
            return "north";
        }
        else if (degrees < 90) {
            return "north-east"
        }
        else if (degrees === 90 || degrees < 135) {
            return "east";
        }
        else if (degrees < 180) {
            return "south-east";
        }
        else if (degrees === 180 || degrees < 225) {
            return "south";
        }
        else if (degrees < 270) {
            return "south-west";
        }
        else if (degrees === 270 || degrees < 315) {
            return "west";
        }
        else if (degrees < 360) {
            return "north-west";
        }
    }
    render() {
        return (
            <div className={Style.parse("module") + " " + Style.parse("weather-module")}>
                <span>Weather Right Now</span>
                <b>West Hartford</b>
                <div className={Style.parse("weather-current")}>
                    <div className={Style.parse("weather-current-temp")}>
                        <p>{this.state.data.query.results.channel.item.condition.temp}&deg;</p>
                        <i className={"wi wi-yahoo-" + this.state.data.query.results.channel.item.condition.code}/>
                    </div>
                    <span>Condition: {this.state.data.query.results.channel.item.condition.text}</span>
                    <span>Humidity: {this.state.data.query.results.channel.atmosphere.humidity}%</span>
                    <span>Wind Speed: {this.state.data.query.results.channel.wind.speed + " mph " + this.degreesToDirection()}</span>
                </div>
                <div className={Style.parse("weather-forecast")}>
                    {this.getForecast()}
                </div>
            </div>
        );
    }

}