export default class User  {
    constructor(googleUser, periods) {
        this.googleUser = googleUser;
        let profile = googleUser.getBasicProfile();
        this.name = profile.getName();
        this.email = profile.getEmail();
        this.image = profile.getImageUrl();
        this.periods = periods;
        let email = this.email.split(".");
        if (email.length === 4) {
            let date = new Date();
            const year = parseInt(email[2].split("@kingswoodoxford.org")[0]);
            const remaining = year - parseInt(String(date.getFullYear()).substring(4, 2));
            if (date.getMonth() > 0 && date.getMonth() < 5) {
                switch (remaining) {
                    case 3 :
                        this.title = ["Freshman, '" + year, 3];
                        break;
                    case 2 :
                        this.title = ["Sophomore, '" + year, 4];
                        break;
                    case 1:
                        this.title = ["Junior, '" + year, 5];
                        break;
                    case 0:
                        this.title = ["Senior, '" + year, 6];
                        break;
                    default:
                        this.title = ["Middle Schooler, '" + year, 0]
                }
            }
            else {
                switch (remaining) {
                    case 4 :
                        this.title = ["Freshman, '" + year, 3];
                        break;
                    case 3 :
                        this.title = ["Sophomore, '" + year, 4];
                        break;
                    case 2:
                        this.title = ["Junior, '" + year, 5];
                        break;
                    case 1:
                        this.title = ["Senior, '" + year, 6];
                        break;
                    default:
                        this.title = ["Middle Schooler, '" + year, 0]
                }
            }
        }
        else {
            this.title =  "Faculty";
        }
    }

    getName() {
        return this.name;
    }

    getImage() {
        return this.image;
    }

    getEmail() {
        return this.email;
    }

    getPeriods() {
        return this.periods;
    }

    getTitle() {
         return this.title[0];
    }

    getNumericalTitle() {
        return this.title[1];
    }

    getGoogleUser() {
        return this.googleUser;
    }

    setPeriods(periods) {
        this.periods = periods;
    }
}