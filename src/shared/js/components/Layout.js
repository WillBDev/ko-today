﻿import React from "react";
import DayView from "./DayView";
import Style from "../Style";
import * as View from "./View";
import DateFormat from "../DateFormat";
import {fetchDate} from "../apis";
import {VelocityTransitionGroup} from "velocity-react";
import Calendar from "./Calendar";
import User from "./User";
Date.prototype.addDays = function (days) {
    let date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
};
let cache = new Map();
let auth2;

export default class Layout extends React.Component {
    constructor(props) {
        super(props);
        cache.set(this.props.date, this.props.data);
        this.state = {view: View.type.DayView, date: this.props.date, toggled: true};
    }

    static updatePageURL(date) {
        let url = "/day/" + date;

        function getDateStr(date) {
            date = DateFormat.convertToObject(date);
            let days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
            let dayName = days[date.getDay()];
            let months = ['January', "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
            let monthName = months[date.getMonth()];
            let dayNumber = date.getDate().toString();
            let target = dayNumber;
            let suffix = "st";
            if (dayNumber.length !== 1) {
                target = dayNumber[dayNumber.length - 1];
            }
            if (parseInt(target) === 2) {
                suffix = "nd";
            }
            else if (parseInt(target) === 3) {
                suffix = "rd";
            }
            else {
                suffix = "th";
            }
            let year = date.getFullYear();
            return dayName + ", " + monthName + " " + dayNumber + suffix + ", " + year;
        }

        let title = "KO Today — " + getDateStr(date);
        if (date === DateFormat.convertToStandard(new Date())) {
            url = "/today";
        }
        document.title = title;
        ga('set', 'page', url);
        ga('send', 'pageview');

        window.history.pushState(null, title, url);
    }

    login() {
        const instance = this;
        fetch("https://kotoday.org/fetch-user", {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                id_token: auth2.currentUser.get().getAuthResponse().id_token,
                name: auth2.currentUser.get().getBasicProfile().getName(),
                email: auth2.currentUser.get().getBasicProfile().getEmail()
            })
        })
            .then((raw) => {
                return raw.json();
            })
            .then((res) => {
                instance.setState({user: (new User(auth2.currentUser.get(), res.periods))});
            }).catch((err) => {
            console.log(err)
        });
    }

    logout() {
        this.setState({user: undefined});
    }

    loadGoogleAPI() {
        const script = document.createElement("script");
        script.onload = () => {
            gapi.load('client:auth2', () => {
                gapi.client.init({
                    apiKey: 'AIzaSyDVz_LbPq9d4fdunlGWYwDrb5pOhfoPwww ',
                    clientId: '640618951759-ghcu52qf2gle28rbm9rqom2115t6sui5.apps.googleusercontent.com',
                    scope: 'profile',
                    hosted_domain: "kingswoodoxford.org"
                }).then(() => {
                    auth2 = gapi.auth2.getAuthInstance();
                    auth2.isSignedIn.listen((bool) => {
                        if (bool) {
                            this.login();
                        }
                        else {
                            this.logout();
                        }
                    });
                    if (auth2.isSignedIn.get()) {
                        this.login();
                    }
                });
            });
        };
        script.src = "https://apis.google.com/js/platform.js";
        document.head.appendChild(script);
    }

    componentDidMount() {
        this.loadGoogleAPI();
        Layout.updatePageURL(this.state.date);
    }

    changeDayState(date) {
        if (this.state.modal) {
            this.setState({modal: undefined});
        }
        this.setState({date: date});
    }

    getDay(date, callback, loadStart) {
        Layout.updatePageURL(date);
        const limit = 50;
        if (!cache.get(date)) {
            if (cache.size - 1 === limit) {
                cache.delete(cache.keys().next().value);
            }
            loadStart();
            fetchDate(date, function (data) {
                cache.set(date, data);
                callback(data);
            });
        }
        else {
            callback(cache.get(date));
        }
    }

    getModal(name, instance) {
        document.getElementsByTagName("html")[0].setAttribute("style", "overflow-y: hidden!important");
        switch (name) {
            case "settings" :

            function getPeriods() {
                let periods = instance.state.user.getPeriods();
                let list = ["A Period", "B Period", "C1 Period", "C2 Period", "D Period", "E Period", "F1 Period", "F2 Period", "G1 Period", "G2 Period", "H Period"];
                let arr = [];

                function quantifyPeriod(name) {
                    if (name === "A Period") {
                        return 0;
                    }
                    else if (name === "B Period") {
                        return 1;
                    }
                    else if (name === "C1 Period") {
                        return 2;
                    }
                    else if (name === "C2 Period") {
                        return 3;
                    }
                    else if (name === "D Period") {
                        return 4;
                    }
                    else if (name === "E Period") {
                        return 5;
                    }
                    else if (name === "H Period") {
                        return 10;
                    }
                    else if (name === "C Period") {
                        return [2, 3];
                    }
                    else if (name === "F Period") {
                        return [6, 7];
                    }
                    else if (name === "G Period") {
                        return [8, 9];
                    }
                    else if (name === "F1 Period") {
                        return 6;
                    }
                    else if (name === "F2 Period") {
                        return 7;
                    }
                    else if (name === "G1 Period") {
                        return 8;
                    }
                    else if (name === "G2 Period") {
                        return 9;
                    }
                    else {
                        return name;
                    }
                }

                function isSpecial(name) {
                    if (name === "C1 Period" || name === "C2 Period") {
                        return "C Period";
                    }
                    else if (name === "F1 Period" || name === "F2 Period") {
                        return "F Period";
                    }
                    else if (name === "G1 Period" || name === "G2 Period") {
                        return "G Period";
                    }
                    return undefined;
                }

                function removeSpace(str) {
                    str.replace(" ", "_");
                }

                for (let i = 0; i < list.length; i++) {
                    let defPeriod = list[i];
                    let spec = isSpecial(defPeriod);
                    let relativeOffset = i;
                    let name;
                    let instructor;
                    let location;
                    let periodNumber;
                    if (spec) {
                        defPeriod = spec;
                        spec = quantifyPeriod(spec);
                        let num;
                        if (periods.length > 0) {
                            let final = quantifyPeriod(periods[spec[0]].name);
                            if (isNaN(final)) {
                                relativeOffset = spec[0];
                                num = 1;
                            }
                            else {
                                relativeOffset = spec[1];
                                num = 2;
                            }
                        }
                        i++;
                        periodNumber = (
                            <div className={Style.parse("form-group")}>
                                <label>Period Number</label>
                                <input type="number" min="1" max="2" name={removeSpace(defPeriod) + "[number]"}
                                       placeholder={"Enter the period number."} defaultValue={num}/>
                            </div>
                        );
                    }

                    if (periods.length > 0) {
                        let custom = periods[relativeOffset];
                        name = custom.name;
                        instructor = custom.instructor;
                        location = custom.location;
                    }
                    arr.push((
                        <div key={defPeriod} className={Style.parse("period-edit")}>
                            <b>{defPeriod}</b>
                            <div className={Style.parse("form-group")}>
                                <label>Course Name:</label>
                                <input type="text" name={removeSpace(defPeriod) + "[name]"}
                                       placeholder={"Enter the course name."}
                                       defaultValue={name}/>
                            </div>
                            {periodNumber}
                            <div className={Style.parse("form-group")}>
                                <label>Instructor:</label>
                                <input type="text" name={removeSpace(defPeriod) + "[instructor]"}
                                       placeholder={"Enter the course instructor."} defaultValue={instructor}/>
                            </div>
                            <div className={Style.parse("form-group")}>
                                <label>Location:</label>
                                <input type="text" name={removeSpace(defPeriod) + "[location]"}
                                       placeholder={"Enter the course location."} defaultValue={location}/>
                            </div>
                        </div>
                    ));
                }
                return arr;
            }

                return (
                    <div className={Style.parse("modal")}>
                        <a className={Style.parse("material-icon") + " " + Style.parse("modal-close")}
                           onClick={() => {
                               document.getElementsByTagName("html")[0].setAttribute("style", "overflow-y: scroll!important");
                               instance.setState({modal: undefined})
                           }}>close</a>
                        <div className={Style.parse("modal-container")}>
                            <h1>Settings</h1>
                            <div className={Style.parse("edit-periods")}>
                                <span>Edit Periods:</span>
                                <form onSubmit={
                                    function (event) {
                                        event.preventDefault();
                                        const items = event.target.elements;
                                        let periods = [];
                                        let isEmpty = () => {
                                            let bool = true;
                                            for (let i = 0; i < items.length; i++) {
                                                if (items[i].value !== "") {
                                                    bool = false;
                                                }
                                            }
                                            return bool;
                                        };
                                        if (!isEmpty()) {
                                            for (let i = 0; i < items.length - 2; i++) {
                                                let name = items[i].value;
                                                if (!name || name === "") {
                                                    name = "Free Period";
                                                }
                                                let number;
                                                let instructor;
                                                let location;
                                                let offset = i;
                                                if (i === 6 || i === 16 || i === 20) {
                                                    number = items[i + 1].value;
                                                    if (number === undefined || number === "") {
                                                        number = 1;
                                                    }
                                                    instructor = items[i + 2].value;
                                                    location = items[i + 3].value;
                                                    offset += 3;
                                                }
                                                else {
                                                    location = items[i + 2].value;
                                                    instructor = items[i + 1].value;
                                                    offset += 2;
                                                }

                                                if (instructor === "") instructor = undefined;
                                                if (location === "") location = undefined;

                                                if (number) {
                                                    let addon;
                                                    if (number === 1) {
                                                        if (i === 6) {
                                                            addon = "C1 Period"
                                                        }
                                                        else if (i === 16) {
                                                            addon = "F1 Period"
                                                        }
                                                        else if (i === 20) {
                                                            addon = "G1 Period"
                                                        }
                                                        periods.push(
                                                            {
                                                                name: addon
                                                            },
                                                            {
                                                                name: name,
                                                                instructor: instructor,
                                                                location: location
                                                            }
                                                        );
                                                    }
                                                    else {
                                                        if (i === 6) {
                                                            addon = "C2 Period"
                                                        }
                                                        else if (i === 16) {
                                                            addon = "F2 Period"
                                                        }
                                                        else if (i === 20) {
                                                            addon = "G2 Period"
                                                        }
                                                        periods.push(
                                                            {
                                                                name: name,
                                                                instructor: instructor,
                                                                location: location
                                                            },
                                                            {
                                                                name: addon
                                                            }
                                                        );
                                                    }
                                                }
                                                else {
                                                    periods.push(
                                                        {
                                                            name: name,
                                                            instructor: instructor,
                                                            location: location
                                                        }
                                                    )
                                                }
                                                i = offset;
                                            }
                                        }

                                        let data = JSON.stringify({
                                            id_token: auth2.currentUser.get().getAuthResponse().id_token,
                                            user: {
                                                name: auth2.currentUser.get().getBasicProfile().getName(),
                                                periods: periods,
                                                email: auth2.currentUser.get().getBasicProfile().getEmail()
                                            }
                                        });
                                        fetch("https://kotoday.org/save-periods", {
                                            method: "POST",
                                            headers: {
                                                'Accept': 'application/json',
                                                'Content-Type': 'application/json'
                                            },
                                            body: data
                                        }).then(() => {
                                            instance.setState({
                                                user: new User(auth2.currentUser.get(), periods),
                                                saved: true
                                            });
                                            document.getElementsByTagName("html")[0].setAttribute("style", "overflow-y: scroll!important");
                                            instance.setState({modal: undefined});
                                            setTimeout(() => {
                                                instance.setState({saved: false})
                                            }, 1250);
                                        });
                                    }
                                }>
                                    {getPeriods()}
                                    <button type="submit">Save Periods</button>
                                </form>
                            </div>
                        </div>
                    </div>
                );
            case "select_date" :
                return (
                    <div className={Style.parse("modal")}>
                        <a className={Style.parse("material-icon") + " " + Style.parse("modal-close")}
                           onClick={() => {
                               document.getElementsByTagName("html")[0].setAttribute("style", "overflow-y: scroll!important");
                               instance.setState({modal: undefined})
                           }}>close</a>
                        <div className={Style.parse("modal-container")}>
                            <h1>Select a Date</h1>
                            <Calendar changeDayState={instance.changeDayState.bind(instance)}
                                      date={instance.state.date}/>
                        </div>
                    </div>
                );
        }
    }

    render() {
        let view;
        let date = this.state.date;
        let user = this.state.user;
        const periodData = (user) ? [user.getPeriods(), user.getNumericalTitle()] : undefined;
        if (this.state.view === View.type.DayView) {
            view = <DayView initial={this.props.data} periods={periodData} date={date}
                            getDay={this.getDay.bind(this)} modal={this.state.modal}
                            changeDayState={this.changeDayState.bind(this)}/>
        }
        let loggedIn = !!this.state.user;

        let userPanel = (loggedIn) ? (
            <ul className={Style.parse("pull-right") + ((!this.state.toggled) ? (" " + Style.parse("open")) : "")}>
                <li className={Style.parse("profile")}>
                    <img draggable="false" src={user.getImage()}
                         alt="profile-image"/>
                    <div className={Style.parse("profile-info")}>
                        <b>{this.state.user.getName()}</b>
                        <span>{this.state.user.getTitle()}</span>
                    </div>
                </li>
                <li><a onClick={(e) => {
                    this.setState({modal: this.getModal('settings', this)})
                }}><i className={Style.parse("material-icon")}>settings</i> Settings</a></li>
                <li><a onClick={() => gapi.auth2.getAuthInstance().signOut()}><i
                    className={Style.parse("material-icon")}>exit_to_app</i> Logout</a></li>
            </ul>) : (
            <ul className={Style.parse("pull-right") + ((!this.state.toggled) ? (" " + Style.parse("open")) : "")}>
                <li><a onClick={() => gapi.auth2.getAuthInstance().signIn()}><i
                    className={Style.parse("material-icon")}>person</i> Sign In</a>
                </li>
            </ul>);

        let modal = this.state.modal;
        let overlay = (modal) ? (<div className={Style.parse("modal-overlay")} onClick={() => {
            document.getElementsByTagName("html")[0].setAttribute("style", "overflow-y: scroll!important");
            this.setState({modal: undefined})
        }}/>) : undefined;
        let saved = (this.state.saved) ? (
            <div className={Style.parse("saved")}><b>Periods Saved!</b></div>) : undefined;
        return (
            <div>
                <VelocityTransitionGroup enter={{
                    animation: {opacity: [1, 0], translateY: [0, 25], scale: [1, 0.6]},
                    duration: 200,
                    easing: [0.0, 0.0, 0.2, 1]
                }} leave={{
                    animation: {opacity: [0, 1], translateY: [25, 0]},
                    duration: 150,
                    easing: [0.4, 0.0, 1, 1]
                }}>
                    {saved}
                </VelocityTransitionGroup>
                <VelocityTransitionGroup enter={{
                    animation: {opacity: [1, 0], translateY: [0, -50]},
                    delay: 300,
                    duration: 250,
                    easing: [0.0, 0.0, 0.2, 1]
                }} leave={{
                    animation: {opacity: [0, 1], translateY: [-50, 0]},
                    duration: 200,
                    easing: [0.4, 0.0, 1, 1]
                }}>
                    {modal}
                </VelocityTransitionGroup>
                <VelocityTransitionGroup enter={{animation: {opacity: [1, 0]}}} leave={{animation: {opacity: [0, 1]}}}>
                    {overlay}
                </VelocityTransitionGroup>
                <nav>
                    <a draggable="false" onClick={() => {
                        this.setState({toggled: !this.state.toggled})
                    }} className={Style.parse("toggler")}><span/><span/><span/></a>
                    <ul className={Style.parse("pull-left") + ((!this.state.toggled) ? (" " + Style.parse("open")) : "")}>
                        <li className={Style.parse("nav-brand")}>
                            <a>
                                <svg data-name="Layer 1" xmlns="https://www.w3.org/2000/svg"
                                     viewBox="0 0 208.82 69.12">
                                    <path className={Style.parse("cls-1")}
                                          d="M49.62,6.19a6.35,6.35,0,0,1,6.2,6.2V55.81a6.35,6.35,0,0,1-6.2,6.2H6.2a5.61,5.61,0,0,1-4.34-1.86A6.08,6.08,0,0,1,0,55.81V12.39a6.35,6.35,0,0,1,6.2-6.2h3V0h6.18V6.19H40.28V0h6.19V6.19Zm0,49.63V21.73H6.19V55.82Z"/>
                                    <path className={Style.parse("cls-1")}
                                          d="M16.63,41.53,14.5,43.87v5.69H9.3V28h5.2V37.5l1.81-2.74L21,28h6.43L20.1,37.53l7.29,12H21.23Z"/>
                                    <path className={Style.parse("cls-1")}
                                          d="M46.53,39.22a12.76,12.76,0,0,1-1.17,5.59A8.67,8.67,0,0,1,42,48.55a10,10,0,0,1-9.81,0A8.57,8.57,0,0,1,28.9,45a12.29,12.29,0,0,1-1.26-5.41V38.34a12.7,12.7,0,0,1,1.17-5.59A8.61,8.61,0,0,1,32.14,29,9.89,9.89,0,0,1,42,29a8.86,8.86,0,0,1,3.34,3.71,12.53,12.53,0,0,1,1.21,5.53Zm-5.29-.91a9,9,0,0,0-1.08-4.88,3.44,3.44,0,0,0-3.07-1.67q-3.92,0-4.14,5.87v1.59A9.39,9.39,0,0,0,34,44.1a3.45,3.45,0,0,0,3.13,1.7,3.39,3.39,0,0,0,3-1.68,9,9,0,0,0,1.09-4.81Z"/>
                                    <path className={Style.parse("cls-2")}
                                          d="M93.06,20.1H81.87v36H72.7v-36h-11V12.81H93.06Z"/>
                                    <path className={Style.parse("cls-2")}
                                          d="M90.39,39q0-7.32,3.61-11.49t10-4.16q6.43,0,10,4.16t3.61,11.55V41.1q0,7.35-3.58,11.48t-10,4.14q-6.47,0-10.06-4.15T90.39,41Zm8.83,2.11q0,8.63,4.82,8.63,4.44,0,4.79-7.2l0-3.54q0-4.41-1.28-6.53a4,4,0,0,0-3.6-2.13,3.9,3.9,0,0,0-3.5,2.13Q99.22,34.57,99.22,39Z"/>
                                    <path className={Style.parse("cls-2")}
                                          d="M121.92,39.05q0-7.92,2.8-11.82a9.4,9.4,0,0,1,8.14-3.89,8,8,0,0,1,6.47,3.21V10.43h8.86V56.12h-8l-.4-3.27a8.2,8.2,0,0,1-7,3.87,9.33,9.33,0,0,1-8.07-3.87Q122,49,121.92,41.48Zm8.83,2.11q0,4.76,1.06,6.65a3.75,3.75,0,0,0,3.57,1.89,4.33,4.33,0,0,0,4-2.24V32.77a4.21,4.21,0,0,0-3.92-2.38,3.81,3.81,0,0,0-3.54,1.87q-1.12,1.88-1.12,6.67Z"/>
                                    <path className={Style.parse("cls-2")}
                                          d="M170.64,56.12a11.25,11.25,0,0,1-.75-2.5,7.61,7.61,0,0,1-6.4,3.1,9.72,9.72,0,0,1-7-2.68,9.18,9.18,0,0,1-2.78-6.93,9.56,9.56,0,0,1,3.37-7.83q3.38-2.76,9.71-2.82h2.68V33.87a4.7,4.7,0,0,0-.78-3.07,2.86,2.86,0,0,0-2.27-.89c-2.2,0-3.29,1.23-3.29,3.69h-8.8a9.15,9.15,0,0,1,3.49-7.36,13.4,13.4,0,0,1,8.85-2.9q5.53,0,8.57,2.75c2,1.83,3,4.46,3,7.87V49.1a15.22,15.22,0,0,0,1.24,6.52v.5Zm-5.19-6a5.2,5.2,0,0,0,2.49-.56,4.2,4.2,0,0,0,1.52-1.34v-6.7h-2.12a4.61,4.61,0,0,0-3.52,1.37,5.14,5.14,0,0,0-1.29,3.66Q162.53,50.11,165.45,50.11Z"/>
                                    <path className={Style.parse("cls-2")}
                                          d="M195,42.11l4.48-18.18h9.39l-11.07,37c-1.66,5.45-4.77,8.18-9.36,8.18a13.77,13.77,0,0,1-3.6-.56V62l1,0a5.23,5.23,0,0,0,3.1-.76,4.75,4.75,0,0,0,1.57-2.63l.68-2.17-9.7-32.52h9.45Z"/>
                                </svg>
                            </a>
                        </li>
                        <li>
                            <a rel="canonical" onClick={(e) => {
                                e.preventDefault();
                                this.setState({date: DateFormat.convertToStandard(new Date())})
                            }} draggable="false"
                               href={"https://kotoday.org/day/" + DateFormat.convertToStandard(new Date())}>
                                <i className={Style.parse("material-icon")}>home</i> Go to today
                            </a>
                        </li>
                        <li>
                            <a onClick={(e) => {
                                e.preventDefault();
                                this.setState({modal: this.getModal("select_date", this)})
                            }} draggable="false">
                                <i className={Style.parse("material-icon")}>event</i> Select date
                            </a>
                        </li>
                        <li>
                            <a className={Style.parse("disabled")}>
                                <i className={Style.parse("material-icon")}>swap_horiz</i> Switch to week view
                            </a>
                        </li>
                    </ul>
                    {userPanel}
                </nav>
                {view}
            </div>
        );
    }
}