import React from "react";
import Style from "../Style";
import DateFormat from "../DateFormat";
import Period from "./Period";
import {VelocityTransitionGroup} from "velocity-react";
import Header from "./Header";
import WeatherModule from "./WeatherModule";

let controller;
let signal;
export default class DayView extends React.Component {

    constructor(props) {
        super(props);
        this.state = {day: this.props.initial.data, loaded: true, quote: this.props.initial.fact.quote, weather: this.props.initial.weather};
    }

    componentWillReceiveProps(newProps) {
        if (newProps !== this.props && newProps.date !== this.props.date) {
            let newDate = DateFormat.convertToObject(newProps.date);
            let oldDate = DateFormat.convertToObject(this.props.date);
            if (newDate > oldDate) {
                this.setState({direction: 1});
            }
            else {
                this.setState({direction: -1});
            }
        }
    }

    componentDidMount() {
        let down = false;
        document.onkeydown = checkKey;
        document.onkeyup = () => {
            down = false
        };
        let $this = this;

        function checkKey(e) {
            if (!down && !$this.props.modal) {
                down = true;
                if (e.keyCode === 37) {
                    $this.goTo(-1);
                }
                else if (e.keyCode === 39) {
                    $this.goTo(1);
                }
            }
        }
    }

    goTo(distance) {
        let date = DateFormat.convertToStandard(DateFormat.convertToObject(this.props.date).addDays(distance));
        this.props.changeDayState(date);
    }

    componentDidUpdate(prevProps) {
        let $this = this;
        if (prevProps.date !== this.props.date) {
            $this.props.getDay(DateFormat.convertToStandard(this.props.date), (data) => {
                $this.setState({day: data.data, quote: data.fact.quote, loaded: true});
            }, () => {$this.setState({loaded: false})});
        }
    }


    render() {
        let date = DateFormat.convertToObject(this.props.date);
        let bool = (this.props.periods && this.props.periods[0]);
        let customPeriods = bool ? this.props.periods[0] : undefined;
        let formNumber = bool ? this.props.periods[1] : undefined;
        function quantifyPeriod(name) {
            if (name === "A Period") {
                return 0;
            }
            else if (name === "B Period") {
                return 1;
            }
            else if (name === "C1 Period") {
                return 2;
            }
            else if (name === "C2 Period") {
                return 3;
            }
            else if (name === "D Period") {
                return 4;
            }
            else if (name === "E Period") {
                return 5;
            }
            else if (name === "H Period") {
                return 10;
            }
            else if (name === "C Period") {
                return [2, 3];
            }
            else if (name === "F Period") {
                return [6, 7];
            }
            else if (name === "G Period") {
                return [8, 9];
            }
            else if (name === "F1 Period") {
                return 6;
            }
            else if (name === "F2 Period") {
                return 7;
            }
            else if (name === "G1 Period") {
                return 8;
            }
            else if (name === "G2 Period") {
                return 9;
            }
            else {
                return name;
            }
        }
        function getPeriods(day, date) {
            let periods = day.periods;
            let desc = "Custom Schedule";
            if (day.day_number !== 0) {
                let week = ((parseInt(day.day_number) / 5) > 1) ? "Week B" : "Week A";
                desc = "Day " + day.day_number + ", " + week;
            }
            let array = [];
            array.push((<p className={Style.parse("day-info")}>{desc}</p>));
            for (let i = 0; i < periods.length; i++) {
                let period = periods[i];
                let name = period.name;
                let instructor;
                let location = period.location;
                let periodValue = quantifyPeriod(name);
                if (customPeriods && customPeriods.length > 0) {
                    if (name === "H1 Period") {
                        if (formNumber > 4) {
                            name = customPeriods[10].name;
                        }
                    }
                    else if (name === "H2 Period") {
                        if (formNumber < 5) {
                            name = customPeriods[10].name;
                        }
                    }
                    else if (!isNaN(periodValue)) {
                        name = customPeriods[periodValue].name;
                        location = customPeriods[periodValue].location;
                        instructor = customPeriods[periodValue].instructor;
                    }
                    else if (Array.isArray(periodValue)) {
                        if (isNaN(quantifyPeriod(customPeriods[periodValue[0]].name))) {
                            name = customPeriods[periodValue[0]].name;
                            location = customPeriods[periodValue[0]].location;
                            instructor = customPeriods[periodValue[0]].instructor;
                        }
                        else {
                            name = customPeriods[periodValue[1]].name;
                            location = customPeriods[periodValue[1]].location;
                            instructor = customPeriods[periodValue[1]].instructor;
                        }
                    }
                }
                array.push(
                    <Period key={date + "_" + i} date={date} startTime={period.start_time} endTime={period.end_time}
                            className={name} defName={period.name} events={period.events} instructor={instructor}
                            location={location}/>
                );
            }
            return array;
        }

        let periods = (this.state.day.periods) ? (getPeriods(this.state.day, this.props.date)) : undefined;
        let noSchool = (!periods && this.state.day.placeholder) ? (
            <div className={Style.parse("no-school")} key={this.props.date}>
                <img draggable="false" alt="excited-gif" src={this.state.day.gif.url}
                     height={(parseInt(this.state.day.gif.dimensions[1]) / parseInt(this.state.day.gif.dimensions[0])) * 250}/>
                <b>{this.state.day.placeholder}</b>
            </div>
        ) : undefined;

        let periodContainer = (this.state.loaded) ? (
            <div id={Style.parse("period-container")} key={this.props.date}>
                {periods}
                {noSchool}
            </div>
        ) : (
            <div className={Style.parse("preloader")} key={this.props.date}>
                <img draggable="false" src={"/img/preloader.gif"}/>
                <b>Loading</b>
            </div>
        );
        return (
            <div className={Style.parse("wrapper")}>
                <Header quote={this.state.quote} date={this.props.date} goTo={this.goTo.bind(this)}/>
                <div>
                    <div id={Style.parse("modules-container")}>
                        <WeatherModule weather={this.state.weather} date={this.props.date} goTo={this.props.changeDayState.bind(this)}/>
                    </div>
                </div>
                <VelocityTransitionGroup enter={{
                    animation: {opacity: [1, 0], translateX: [0, this.state.direction * 50]},
                    delay: 300,
                    duration: 250,
                    easing: [0.0, 0.0, 0.2, 1]
                }} leave={{
                    animation: {
                        opacity: [0, 1],
                        translateX: [this.state.direction * -50, 0]
                    }, duration: 200, easing: [0.4, 0.0, 1, 1]
                }}>
                    {periodContainer}
                </VelocityTransitionGroup>
            </div>)
    }
}