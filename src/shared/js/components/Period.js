import Style from "../Style";
import React from "react";
import DateFormat from "../DateFormat";

export default class Period extends React.Component {

    constructor() {
        super();
        this.interval = undefined;
        this.state = {remaining: undefined, dots: undefined};
    }

    update() {
        if (this.props.date === DateFormat.convertToStandard(new Date())) {
            let ultimateDuration = DateFormat.compare(this.props.startTime, this.props.endTime);
            let goal = ultimateDuration / 6;
            if (new Date() < DateFormat.standardToDate(this.props.endTime) && new Date() > DateFormat.standardToDate(this.props.startTime)) {
                let duration = DateFormat.preciseCompare(new Date(), this.props.endTime);
                let base = new Date();
                let minutes = DateFormat.compare(base.getHours() + ":" + base.getMinutes(), this.props.endTime);
                let checkpoint = Math.floor((ultimateDuration - minutes) / goal);
                this.setState({remaining: duration, dots: checkpoint})
            }
            else if (this.state.remaining) {
                this.setState({remaining: undefined, dots: undefined})
            }
        }
    }

    componentDidUpdate(prevProps) {
        if (this.props !== prevProps) {
            clearInterval(this.interval);
            this.update();
            this.interval = setInterval(() => {
                this.update();
            }, 1000);
        }
    }


    componentDidMount() {
        this.update();
        this.interval = setInterval(() => {
            this.update();
        }, 1000);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    };

    render() {
        let flexWarning = (DateFormat.compare(this.props.startTime, this.props.endTime) === 75) ? (
            <div className={Style.parse("flex-warning")}>
                <i className={Style.parse("material-icon")}>info</i>
                <b>Flex</b>
            </div>
        ) : undefined;

        let location = (this.props.location) ? (
            <div className={Style.parse("icon-group")}>
                <div>
                    <i className={Style.parse("material-icon")}>location_on</i>
                    <span>{this.props.location}</span>
                </div>
            </div>
        ) : undefined;

        let instructor = (this.props.instructor) ? (
            <div className={Style.parse("icon-group")}>
                <div>
                    <i className={Style.parse("material-icon")}>person</i>
                    <span>{this.props.instructor}</span>
                </div>
            </div>
        ) : undefined;

        function getEvents(props) {
            let events = props.events;
            let eventDom = [];
            let groupObj;
            for (let i = 0; i < events.length; i++) {
                let event = events[i];
                let name = (event.name) ? (
                    <div className={Style.parse("icon-group")}>
                        <div>
                            <i className={Style.parse("material-icon")}>event</i>
                            <span>{event.name}</span>
                        </div>
                    </div>
                ) : undefined;
                if (event.group) {
                    if (Array.isArray(event.group)) {
                        let arr = event.group;
                        let final = [];
                        for (let i = 0; i < arr.length; i++) {
                            let item = arr[i];
                            final.push(<div className={Style.parse("icon-group")}>
                                <div>
                                    <i className={Style.parse("material-icon")}>group</i>
                                    <span>{item}</span>
                                </div>
                            </div>);
                        }
                        groupObj = final;
                    }
                    else {
                        groupObj = (<div className={Style.parse("icon-group")}>
                            <div>
                                <i className={Style.parse("material-icon")}>group</i>
                                <span>{event.group}</span>
                            </div>
                        </div>)
                    }
                }
                let group = (groupObj) ? (
                    <React.Fragment>{groupObj}</React.Fragment>
                ) : undefined;
                let description = (event.description) ? (
                    <div className={Style.parse("icon-group")}>
                        <div>
                            <i className={Style.parse("material-icon")}>description</i>
                            <span>{event.description}</span>
                        </div>
                    </div>
                ) : undefined;
                let location = (event.location) ? (
                    <div className={Style.parse("icon-group")}>
                        <div>
                            <i className={Style.parse("material-icon")}>location_on</i>
                            <span>{event.location}</span>
                        </div>
                    </div>
                ) : undefined;
                eventDom.push(<div className={Style.parse("event")} key={i}>{name}{group}{description}{location}</div>);
            }
            return eventDom;
        }

        let events = (this.props.events) ? getEvents(this.props) : undefined;
        if (this.props.events) {
            events = (
                <div className={Style.parse("events-container")}>
                    <b>Events:</b>
                    {events}
                </div>
            );
        }
        let secondary = (events) ? (
            <div className={Style.parse("secondary-container")}>
                {events}
            </div>) : undefined;

        let remaining = (this.state.remaining) ? (
            <div className={Style.parse("time-remaining")}>
                <b>{this.state.remaining}</b>
                <span>Remaining</span>
            </div>) : undefined;

        let defName = (this.props.className !== this.props.defName) ? (<span>({this.props.defName})</span>) : undefined;

        function genDots($this) {
            let dots = [];
            let dynNum = ($this.state.dots) ? $this.state.dots : 0;
            for (let i = 0; i < dynNum; i++) {
                dots.push(<div key={i} className={Style.parse("active")}/>);
            }
            for (let i = 0; i < (5 - dynNum); i++) {
                dots.push(<div key={i}/>);
            }
            if ($this.state.dots === 6) {
                dots.push(<div key={"chev"}
                               className={Style.parse("material-icon") + " " + Style.parse("active")}>chevron_right</div>);
            }
            else {
                dots.push(<div key={"chev"} className={Style.parse("material-icon")}>chevron_right</div>);
            }
            return dots;
        }

        let lunch = (this.props.className.includes("Lunch")) ? (
            <div className={Style.parse("icon-group")}>
                <div>
                    <i className={Style.parse("material-icon")}>room_service</i>
                    <a rel="noreferrer" href="https://www.sagedining.com/menus/kingswoodoxford" target="_blank">View
                        Menu</a>
                </div>
            </div>
        ) : undefined;
        return (
            <div className={Style.parse("period")}>
                <div className={Style.parse("primary-container")}>
                    {flexWarning}
                    <div className={Style.parse("left")}>
                        <div className={Style.parse("time-gauge")}>
                            <span>{DateFormat.normalizeStandard(this.props.startTime)}</span>
                            <div className={Style.parse("dots")}>
                                {genDots(this)}
                            </div>
                            <span>{DateFormat.normalizeStandard(this.props.endTime)}</span>
                        </div>
                        <div className={Style.parse("period-name")}>
                            <b>{this.props.className}</b> {defName}
                        </div>
                        {lunch}
                        {instructor}
                        {location}
                    </div>
                    <div className={Style.parse("center")}>
                        {(this.state.remaining) ? (<b>NOW</b>) : undefined}
                    </div>
                    <div className={Style.parse("right")}>
                        {remaining}
                    </div>
                </div>
                {secondary}
            </div>
        );
    }
}