import Style from "./Style";

let controller;
let signal;
let timer;
export function fetchDate(date, callback) {

    if (controller !== undefined) {
        controller.abort();
    }

    if (timer) {
        clearTimeout(timer);
    }

    if ("AbortController" in window) {
        controller = new AbortController;
        signal = controller.signal;
    }
    fetch("https://kotoday.org/fetch-date/" + date, {signal})
        .then((response) => response.json())
        .then((result) => {
            callback(result);
        }).catch((err)=>{
        if (err.name !== 'AbortError') {
            timer = setTimeout(()=>{fetchDate(date, callback)}, 1000);
        }
    });
}