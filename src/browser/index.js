import Layout from '../shared/js/components/Layout';
import React from "react";
import serverStyleCleanup from 'node-style-loader/clientCleanup'
import {hydrate} from 'react-dom';
import * as apis from "../shared/js/apis";
import Style from "../shared/js/Style";
const preloadedData = window.__INITIAL_DATA__;
delete window.__INITIAL_DATA__;
const FastClick = require('fastclick');
hydrate(
    <Layout date={preloadedData.date} data={preloadedData.data}/>
    , document.getElementById("app"));
let image = new Image();
image.src = 'https://kotoday.org/img/preloader.gif';
let start = new Date();
var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification));
if (!isSafari) {
window.addEventListener("load", () => {
    FastClick.attach(document.body, {});
    let end = new Date();
    if ((end-start)/1000 >= 1000) {
        document.getElementsByClassName(Style.parse("preloader-main"))[0].classList.add(Style.parse("loaded"));
        setTimeout(()=> document.getElementsByTagName("html")[0].classList.add(Style.parse("loaded")), 300);
    } else {
        let remaining = (1-(((end-start)/1000)/1000))*1000;
        setTimeout(()=> {
            document.getElementsByClassName(Style.parse("preloader-main"))[0].classList.add(Style.parse("loaded"));
            setTimeout(()=> document.getElementsByTagName("html")[0].classList.add(Style.parse("loaded")), 300);
        }, remaining)
    }
    serverStyleCleanup();
}, false);
}
else {
    var _timer=setInterval(function(){
        if(/loaded|complete/.test(document.readyState)){
            clearInterval(_timer)
            FastClick.attach(document.body, {});
    let end = new Date();
    if ((end-start)/1000 >= 1000) {
        document.getElementsByClassName(Style.parse("preloader-main"))[0].classList.add(Style.parse("loaded"));
        setTimeout(()=> document.getElementsByTagName("html")[0].classList.add(Style.parse("loaded")), 300);
    } else {
        let remaining = (1-(((end-start)/1000)/1000))*1000;
        setTimeout(()=> {
            document.getElementsByClassName(Style.parse("preloader-main"))[0].classList.add(Style.parse("loaded"));
            setTimeout(()=> document.getElementsByTagName("html")[0].classList.add(Style.parse("loaded")), 300);
        }, remaining)
    }
    serverStyleCleanup();
        }}
    , 10)
}
'use strict';

const testWepP = callback => {
    let webP = new Image();

    webP.src = 'data:image/webp;base64,UklGRi4AAABXRUJQVlA4TCEAAAAvAUAAEB8wA' +
        'iMwAgSSNtse/cXjxyCCmrYNWPwmHRH9jwMA';
    webP.onload = webP.onerror = () => {
        callback(webP.height === 2);
    };
};

const addWebPClass = (support) => {
    if (!support) {
        let el = document.body;

        if (el.classList) {
            el.classList.add(Style.parse('nowebp'));
        } else {
            el.className += " " + Style.parse('nowebp');
        }
    }
};

document.addEventListener('DOMContentLoaded', testWepP(addWebPClass));