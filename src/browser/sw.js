const staticAssets = ['./bundle.js', './', './img/bk.jpg', './img/bk.webp', './img/preloader.gif'];

self.addEventListener('install', async event => {
    const cache = await caches.open('kotoday-static');
    cache.addAll(staticAssets);
});

self.addEventListener('fetch', event => {
    const req = event.request;
    const url = new URL(req.url);
    if (url.origin === location.origin) {
        event.responseWith(cacheFirst(req));
    }
    else {
        event.responseWith(networkFirst(req));
    }
});

async function cacheFirst(req) {
    const cachedResponse = await caches.match(req);
    return cachedResponse || fetch(req);
}

async function networkFirst(req) {
    const cache = await caches.open("kotoday-dynamic");
    try {
        const res =await fetch(req);
        cache.put(req, res.clone());
        return res;
    } catch (error) {
        return await cache.match(req);
    }
}